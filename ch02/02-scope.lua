-- Scope
-- Dog is accessable in the global chunk
dog = "This is a dog"

-- Cat is accessable in the file chunk
-- The local keyword makes cat local to the file
local cat = "This is a cat"

do -- Do / end will be discussed next
    -- Fish is in a local chunk, in this example
    -- that means local to the do/end block
    local fish = "This is a fish"
    print(fish)
end
print(dog)
print(cat)
print(fish)

foo = 7 -- global scope
do
    local bar = 8 -- local scope
    print ("foo: " .. foo)
    print ("bar: " .. bar)
    do
        local x = 9 -- nested local
        -- can access foo, bar and x
    end
end
print ("foo: " .. foo)
-- print ("bar: " .. bar) -- error!
