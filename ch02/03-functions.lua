print ('about to declare the PrintSomething function');

function PrintSomething() -- declare the function
    text1 = "hello"
    text2 = "world"
    print (text1 .. ", " .. text2)
end

print ('the PrintSomething function is declared');
print ('calling the PrintSomething function');

PrintSomething(); -- call the function

print ('called the PrintSomething function');
print()

-- function arguments

-- Declare the function, takes two arguments
function AddAndPrint(x, y)
    local result = x + y;
    print (x .. "+" .. y .. "=" .. result)
end

-- Call the function a few times
AddAndPrint(2, 3)
AddAndPrint(4, 5)
AddAndPrint(6, 7)

-- Any number of arguments
-- Call the function a few times, extra arguments will be ignored
AddAndPrint(2, 3, 7) -- Will print 2+3=5
AddAndPrint(4, 5, 8, 9, 10) -- Will print 4+5=9
AddAndPrint(6, 7, 11, 12, 14) -- Will print 6+7=13

-- Declare the function, takes two arguments
function PrintValues(x, y)
    print ("x: " .. tostring(x) .. ", y: " .. tostring(y))
end


-- Call the function a few times, the missing variables will get a value of nil
PrintValues(3, 4) -- will print x: 3, y: 4
PrintValues(1) -- will print x: 1, y: nil
PrintValues() -- will print x: nil, y: nil
print()

-- Returning a value
-- declare the function
function AddTwo(x)
    result = x + 2
    print (x .. " + 2 = " .. result)
    return result
end

AddTwo(3) -- calls as statement
nine = 7 + AddTwo(5) -- Call as expression
print ("adding two " .. AddTwo(3)) -- Call as expression

-- Declare the function
function SquareIt(number)
    result = number * number
    print ("this will print") -- WILL PRINT!
    do
        return result
    end
    print ("this will not print") -- WILL NOT PRINT
end

-- Call the function
four = SquareIt(2) -- Will print: this will print
print(four) -- Will print: 4

-- Returning multiple values
-- Declare the function
function SquareAndCube(x)
    squared = x * x
    cubed = x * x * x
    return squared, cubed
end

-- Call the function
s, c = SquareAndCube(2)
print ("Squared: " .. s) -- will print: Squared: 4
print ("Cubed: " .. c) -- will print: Cubed: 8

-- The extra variables will have a default value of nil
s, c, q = SquareAndCube(2) -- Call the same function
print ("Squared: " .. s) -- will print: Squared: 4
print ("Cubed: " .. c) -- will print: Cubed: 8
print ("Quartic: " .. tostring(q)) -- will print: Quartic: nil

square = SquareAndCube(2) -- Call the same function
-- rest of results are discarded
print ("Squared: " .. square) -- will print: Squared: 4
