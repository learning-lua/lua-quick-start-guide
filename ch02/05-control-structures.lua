-- Control structures
-- if

print ("Enter your name")
name = "Edu" -- io.read()

if #name <= 3 then
    print ("that's a short name, " .. name)
end

-- elseif
if #name <= 3 then
    print ("that's a short name, " .. name)
elseif #name <= 6 then
    print (name .. " is an average length name")
end

print ("Enter a number")
x = "2" -- io.read()

if x == "0" then
    print ("input is 0!")
elseif x == "1" then
    print ("input is 1!")
elseif x == "2" then
    print ("input is 2!")
elseif x == "3" then
    print ("input is 3!")
end

-- else
if #name <= 3 then
    print ("that's a short name, " .. name)
elseif #name <= 6 then
    print (name .. " is an average length name")
else
    print ("that's a long name, " .. name)
end

print ("Enter a number")
x = 5 -- io.read()

if x % 2 == 0 then
    print (x .. " is even")
else
    print (x .. " is odd")
end

-- Nesting if statements
if x == "6" then
    print ("x is six!")

    print ("Enter another number")
    local y = io.read()
    -- Nested if statement begins here
    if y == "6" then
        print ("y is also six!")
    elseif y == "5" then
        print ("y is one less than x")
    else
        print ("x is 6, but y is not!")
    end
    -- Nested if statement ends here
else
    print ("x is not 6! " .. x)
end
print()

-- Loops
-- while loops
x = 10 -- Initialize a "control" variable

while x > 0 do -- Boolean condition: x > 0
    print ("hello, world")

    x = x - 1 -- Decrement the "control" variable
end
print()

-- Breaking a loop
x = 0

while x < 10 do -- Execute 10 times!
    print ("x is " .. x)

    if x == 5 then
        -- This stops the loop execution at 5
        break
    end

    x = x + 1
end
print()

function Foo() -- Declare Foo
    local x = 0
    while x < 10 do
        if x == 5 then
            break -- Stop executing the while loop
        end -- end if x == 5
        x = x + 1
    end -- end while x < 10

    -- This print statement will execute
    print ("x is " .. x)

    local y = 0
    while y < 10 do
        if y == 5 then
            return y -- Stop executing the function
        end -- end if y == 5
        y = y + 1
    end -- end while y < 10

    -- This print statement will NOT execute, because of the return statement
    print ("y is " .. y)

end -- end function Foo

-- Call the function Foo
Foo()

-- Repeat until loop
x = 10
repeat
    print ("Repeat loop")
until x > 2
print()

-- for loop
for i = 0, 10, 1 do
    print ( i )
end
print()

-- step by 2
for i = 0, 10, 2 do
    print ( i )
end
print()

-- count down
for i = 10, 0, -1 do
    print ( i )
end
print()

-- default step is 1
for i = 0, 10 do
    print ( i )
end
print()

-- Nested loops
for i = 0, 10 do
    local j = 0
    while j < 10 do
        print ("j: " .. j) -- Will never be > 2
        if j == 2 then
            print ("j is: " .. j .. ", i is:" .. i)
            break
        end
        j = j + 1
    end
end
