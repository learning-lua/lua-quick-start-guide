-- Operators
x = -7 -- negation operator applied to the constant 7
y = -x -- negation operator applied to the x variable
-- x: -7, y: 7
print(x)
print(y)

x = 7 - 3 -- Operand 1 is the constant 7, Operand 2 is the constant 3
y = x - 1 -- Operand 1 is the variable x, Operand 2 is the constant 1
z = x - y -- Operand 1 is the variable x, Operand 2 is the variable y
print(x)
print(y)
print(z)
print()

-- Arithmetic operators
-- addition
x = 7 + 10
y = x + 3
z = x + y
print(x)
print(y)
print(z)

-- subtraction
x = 8 - 3
y = 10 - x
z = x - y
print(x)
print(y)
print(z)

-- multiplication
x = 2 * 2
y = x * 3
z = x * y
print(x)
print(y)
print(z)

-- division
x = 20 / 10
y = 5 / x
z = x / y
print(x)
print(y)
print(z)

-- modulus
x = 5 % 2 -- result is 1
y = 5.7 % 2 -- 5.7 is treated as 5, result is 1.
z = 5.3 % 2.9 -- result is 1
print(x)
print(y)
print(z)

-- negation
x = -5 -- x = -5
y = -x -- y = 5
y = -y -- y = -5
print(x)
print(y)
print(z)

-- exponent
x = 10 ^ 2
y = x ^ 2
z = 3 ^ 3
print(x)
print(y)
print(z)
print()

-- Relational operators
-- equality
x = 2 == 2 -- true
y = 2 == 3 -- false
z = "nine" == 9 -- false
print(x)
print(y)
print(z)

-- inequality
x = 2 ~= 2 -- false
y = 2 ~= 3 -- true
z = "nine" ~= 9 -- true
print(x)
print(y)
print(z)

-- greater than
x = 4 > 5 -- false
y = 4 > 4 -- false
z = 4 > 3 -- true
print(x)
print(y)
print(z)

-- greater than or equal to
x = 4 >= 5 -- false
y = 4 >= 4 -- true
z = 4 >= 3 -- true
print(x)
print(y)
print(z)

-- less than
x = 3 < 2 -- false
y = 3 < 3 -- false
z = 3 < 4 -- true
print(x)
print(y)
print(z)

-- less than or equal to
x = 3 <= 2 -- false
y = 3 <= 3 -- true
z = 3 <= 4 -- true
print(x)
print(y)
print(z)
print()

-- Logical operators
-- and
x = true and false -- value is false
y = false and false -- value is false
z = true and true -- value is true
w = 7 and 1 -- value is 1
print(x)
print(y)
print(z)
print(w)

-- or
x = true or false -- value is true
y = false or false -- value is false
z = true or true -- value is true
w = 7 or 1 -- value is 7
print(x)
print(y)
print(z)
print(w)

function TrueFunction()
    print ("returning true")
    return true
end

function FalseFunction()
    print ("returning false")
    return false
end

x = FalseFunction() and TrueFunction()
print(x)
x = TrueFunction() and FalseFunction()
print(x)

-- not
x = not true -- false
y = not true or false -- false
z = not not false -- false
w = not (7 + 1) -- false
print(x)
print(y)
print(z)
print(w)
print()

-- Misc operators
-- assignment
x = 2
y, z = 4, "hello"
print(x)
print(y)
print(z)

-- string concatenation
hello = "hello,"
world = " world"
print (hello .. world)

-- length
print ("Enter a word: ")
word = io.read();
print (word .. " has " .. #word .. " letters!")
print()

-- Operator precedence
print ( 5 + 2 * 10 ) -- prints 25
print ( (5 + 2) * 10 ) -- prints 70
