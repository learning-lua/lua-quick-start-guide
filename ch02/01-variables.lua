-- variables in Lua
foo = "bar"
print(foo)
-- comments
-- print (foo)
-- The above statement never prints
-- because it is commented out.
print()

-- variables can be reassigned and type also can be changed
color = "red"
print(color)
color = "green"
print(color)
color = "blue"
print(color)
color = 255
print(color)
print()

-- Basic types in Lua:
-- * nil: The absence of data.
-- * Boolean: A value of true or false.
-- * number: A number can represent any real number: 0, -1, 5, or even decimals such as 3.14159265359.
-- * string: A string is an array of characters.
-- * function: A function is some code that is referred to by a name and can be executed any time.
-- * table: A table contains information using key-value pairs.
-- * userdata: Complex data structures defined in the C programming language.
-- * thread: Threads can be used to execute code in parallel.

-- nil
print(bar)
bar = "foo"
print(bar)
bar = nil
print(bar)
print()

-- Boolean
foo = true
print("The value of foo is:")
print(foo)
result = 5 > 3
print("Is 5 > 3?")
print(result)
print()

-- number
pi = 3.1415
three = math.floor(3.1415)
five = math.ceil(4.145)
print (pi) -- will print: 3.1415
print (three) -- will print: 3
print (five) -- will print: 5
five = 3 + 2
print (five) -- will print 5
print (2 + 2) -- will print 4
print (five + 1) -- will print 6
print()

-- Finding a type
var1 = true
var2 = 3.145
var3 = nil
var4 = type(var1)
var5 = type(type(var2))

print (type(var1)) -- boolean
print (type(var2)) -- number
print (type(var3)) -- nil
print (var4) -- boolean
print (var5) -- string
print()

-- String types
print ("Print a string literal, used in place")
message = "Print a string assigned to a variable"
print(message)

-- length of the string
hello = "hello, world"
-- Assign length to variables
count_hash = #hello;
count_func = string.len(hello)
print ("The string:")
print (hello)
-- Print the variables assigned at the top
print ("Has a length of:")
print (count_hash)
print(count_func)
-- Use string literals, in place
print (#"hello, world")
print (string.len("hello, world"))

-- concatenating strings
name = "Mike"
color = "Blue"
-- Concatenate three strings
print ("Jill " .. "likes" .. " Red")
-- Concatenate a variable and a strings
print ("Jack dislikes " .. color)
-- Concatenate two variables and a string
print (name .. " likes " .. color)
-- Concatenate only variables
print (name .. color)
-- Assign result to variable
message = name .. " likes " .. color
print (message)

-- String coercion
pi = 3.14
message = "The rounded value of pi is: " .. pi
print(message)
print("Nine: " .. 9)

eleven = "10" + 1
print (eleven)
print (7 + "01") -- 8

-- Escape characters
message = 'he said "bye" and left' -- This is not desirable.
print (message)
message = "he said \"bye\" and left" -- As a convention, only double quotes should be used to represent a string
print (message)

-- Console input
print ("Please enter your name:")
name = io.read()
print ("Hello " .. name)
