-- Operators for tables and arrays
-- Math operators
meta = {
    __add = function(left, right)
        -- Do stuff
    end
}
-- mathematical meta method:
-- __add: Addition, when writing "table + object" or "object + table"
-- __sub: Subtraction, when writing "table - object" or "object - table"
-- __mul: Multiplication, when writing "table * object" or "object * table"
-- __div: Division, when writing "table / object" or "object / table"
-- __mod: Modulo, when writing "table % object" or "object % table"
-- __pow: Involution, when writing "table ^ object" or "object ^ table"

-- Equivalence operators
-- __eq: Check for equality, when table1 == table2 is evaluated
-- __lt: Check for less than, when table1 < table2 is evaluated
-- __le: Check for less than or equal to, when table1 <= table2 is evaluated

-- Other operators
-- __tostring: Expected to return a string representation of a table.
-- __len: Expected to return the length of the table when writing #table.
-- __concat: Expected to concatenate two tables when writing table1 .. table2.
