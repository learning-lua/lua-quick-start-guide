-- Iterating
-- Understanding pairs
vector = { x = 34, y = 22, z = 56 }

for k, v in pairs(vector) do
    print ("key: " .. k .. ", value: " .. v)
end
print()

-- Understanding ipairs
days = { "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" }

for i, v in ipairs(days) do
    print ("index: " .. i .. ", value: " .. v)
end
print()

-- Closures
function NextNumber()
    -- local to the NextNumber function
    local currentNumber = 0

    return function () -- anonymous function
        -- Because this anonymous function is
        -- created inside the NextNumber function
        -- it can see all members of NextNumber
        -- this function will remember the state
        -- of NextNumber, creating a closure!

        currentNumber = currentNumber + 1

        return currentNumber
    end
end

-- Assign the anonymous function to the variable next
next = NextNumber()
-- At this point, NextNumber has finished executing.

print (next()) -- call anonymous function using next
print (next()) -- call anonymous function using next
print (next()) -- call anonymous function using next

-- currentNumber does not exist in a global context!
print (currentNumber) -- will print nil
print()

-- Iterator functions
days = { "monday", "tuesday", "wednesday", "thursday" }

function walk(array)
    local index = 0

    return function()
        index = index + 1

        return array[index]
    end
end

for day in walk(days) do
    print (day)
end
print()

-- Meta tables
meta = { } -- Creates table
meta.__add = function(left, right) -- adds meta method
    return left.value + right -- left is assumed to be a table.
end

container = {
    value = 5
}

-- result = container + 4 -- ERROR
setmetatable(container, meta) -- Set meta table
result = container + 4 -- Works!
print ("result: " .. result)
print()

-- setmetatable
container = {
    value = 5,
    __add = function(l, r)
        return l.value + r.value
    end
}

setmetatable(container, container)
result = container + container
print ("result: " .. result)
print()

-- getmetatable
x = {}
y = {}
z = {}

setmetatable(y, z)

print (getmetatable(x))
print (getmetatable(y))
print()

-- __index
x = {
    foo = "bar"
}
y = { }

print (x.foo) -- bar
print (x.hello) -- nil
print (y.foo) -- nil
print (y.hello) -- nil
print()

x = {
    foo = "bar"
}

y = { }

z = {
    hello = "world z",
    __index = function(table, key)
        return z[key]
    end
}

w = {
    __index = function(table, key)
        if key == "hello" then
            return "inline world"
        end
        return nil
    end
}

setmetatable(x, z)
setmetatable(y, w)

print (x.foo) -- bar
print (x.hello) -- world z

print (y.foo) -- nil
print (y.hello) -- inline world
print()

-- __newindex
x = { }
y = { }

z = {
    __index = function(table, key)
        return z[key]
    end,
    __newindex = function(table, key, value)
        z[key] = value
    end
}

setmetatable(x, z)
setmetatable(y, z)

x.foo = "bar"

print (x.foo)
print (y.foo)
print (z.foo)
print()

-- rawget and rawset
x = { }
y = { }

z = {
    __index = function(table, key)
        return z[key]
    end,
    __newindex = function(table, key, value)
        z[key] = value
    end
}

setmetatable(x, z)
setmetatable(y, z)

x.foo = "bar" -- Sets "bar" in z
rawset(x, "foo", "raw") -- Sets "raw" in x directly!
print (x.foo) -- raw, lives in x
print (y.foo) -- bar, lives in z
print()

-- __call
tbl = {
    __call = function(table, val1, val2)
        return "Hello, from functor: " .. (val1 + val2)
    end
}

setmetatable(tbl, tbl)
message = tbl(2, 3); -- Calling the table like a function!
print ("message: " .. message)
print()
