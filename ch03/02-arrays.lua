-- Arrays
arr = {}

arr[1] = "x"
arr[2] = "y"
arr[3] = "z"

for i = 1, 3 do
    print(arr[i])
end
print()

-- Array constructor
arr = { "monday", "tuesday", "wednesday" }

for i = 1, 3 do
    print(arr[i])
end
print()

-- Arrays are one-based
vector = { "x", "y", "z" }

print(tostring(vector[0])) -- nil, the array starts at 1
print(vector[1]) -- first element, x
print(vector[2]) -- second element, y
print(vector[3]) -- third element, z

-- can explicitly place a value in index 0
vector = { [0] = "x", "y", "z", "w" } -- this is not recommended to use 0 as an array index

print(vector[0]) -- element before first, x
print(vector[1]) -- first element, y
print(vector[2]) -- second element, z
print(vector[3]) -- third element, w
print()

-- Sparse arrays
arr = { }

arr[1] = "x"
arr[2] = "y"
-- arr[3] is nil by default
-- arr[4] is nil by default
arr[5] = "z"
arr[6] = "w"

for i = 1, 6 do
    print(tostring(arr[i]))
end
print()

-- The size of an array
arr = { "a", "b", "c", "d", "e", "f", "g" }

length = #arr
print("array length: " .. length)

for i = 1, #arr do
    print(arr[i])
end
print()

arr = { }

arr[0] = "x" -- not counted towards length
arr[1] = "y"
arr[2] = "z"

length = #arr -- length = 2!
print("array length: " .. length)

-- to find the length of a sparse array is tricky
arr = { }

arr[1] = "x"
arr[2] = "y"
-- Skipping 3 & 4, at least 2 nils after each other end the array
arr[5] = "z" -- not counted towards length
arr[6] = "w" -- not counted towards length

-- the # operator to find the length of an array is considered to be unreliable
length = #arr -- length = 2, which is WRONG!
print("array length: " .. length)
print()

-- Multidimensional arrays
num_rows = 4
num_cols = 4

matrix = {} -- create new matrix
for i = 1, num_rows do
    matrix[i] = {} -- create new row
    for j = 1, num_cols do
        matrix[i][j] = i * j -- Assign value to row i, column j
    end
end

num_rows = 4
num_cols = 4

values = { 'A', 'B', 'C', 'D',
           'E', 'F', 'G', 'H',
           'I', 'J', 'K', 'L',
           'M', 'N', 'O', 'P'}
value = 1

matrix = {} -- create new matrix

for i=1,num_rows do
    matrix[i] = {} -- create new row

    for j=1,num_cols do
        -- current element: row i, column j
        -- assign current value to element
        matrix[i][j] = values[value] -- assign element to column

        value = value + 1 -- move to next letter
    end
end

-- print some elements
print (matrix[1][1])
print (matrix[2][4])
print (matrix[3][4])
