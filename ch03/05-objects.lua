-- Classes
Enemy = { }
Enemy.health = 200
Enemy.attack = 4
Enemy.defense = 20

-- By convention, the first argument should be names self.
-- The reason for this will be explained later in this section
Enemy.new = function (self, object)
    object = object or {} -- Use provided table, or create new one
    setmetatable(object, self) -- Assign meta table
    self.__index = self
    return object
end

grunt = Enemy.new(Enemy) -- Health is stored in "Enemy"
miniBoss = Enemy.new(Enemy) -- Health is stored in "Enemy"
boss = Enemy.new(Enemy, { health = 500, defense = 100 } ) -- Health is stored in "boss"

miniBoss.health = 250 -- Health is now stored in "miniBoss"

-- grunt does not have a health variable, so the enemy table health is returned
print ("grunt health: " .. grunt.health)
-- miniBoss has a health variable, it was created in the above assignment
print ("mini boss health: " .. miniBoss.health)
-- boss also has a health variable, so the boss table health is returned
print ("boss health: " .. boss.health)
print()

-- By convention, the first argument should be names self
-- The reason for this will be explained later in this section
Enemy.hit = function(self, damage)
    damage = damage - self.defense
    if damage < 0 then
        damage = 0
    end
    self.health = self.health - damage
end

print ("Hero attacks both boss and grunt")

Enemy.hit(boss, 50)
Enemy.hit(grunt, 55)

print ("grunt health: " .. grunt.health)
print ("boss health: " .. boss.health)
print()

-- The : operator
Vector = {
    x = 0,
    y = 1,
    z = 0
}

Vector.new = function (self, object)
    object = object or {} -- Use provided table, or create new one
    setmetatable(object, self) -- Assign meta table
    self.__index = self
    return object
end

Vector.print = function(self)
    print("x:" .. self.x .. ", y: " .. self.y .. ", z: " .. self.z)
end

-- same as Vector.new(Vector, nil)
velocity = Vector:new()

-- Same as Vector.new(Vector, {x=20,z=10})
momentum = Vector:new({x = 20, z = 10})

-- Using the dot syntax, the print method of the
-- Vector class is called, and the object instance
-- is passed as it's first variable (self)
Vector.print(velocity)
Vector.print(momentum)

-- Using the colon syntax, the print method can be
-- called on instances of the Vector class. The colon
-- operator will fill in the first variable (self), with
-- the object instance it is being called on
velocity:print()
momentum:print()
print()

-- previous grunt and boss rewritten with : operator
grunt = Enemy:new() -- self = Enemy
boss = Enemy:new({ health = 500, defense = 100 } ) -- self = Enemy
print ("grunt health: " .. grunt.health)
print ("boss health: " .. boss.health)
boss:hit(50)
grunt:hit(55)
print ("grunt health: " .. grunt.health)
print ("boss health: " .. boss.health)
print()

-- Tables inside of objects
Character = {
    alive = true
}

Character.position = {
    x = 10, y = 20, z = 30
}

Character.new = function(self, object)
    object = object or {}
    setmetatable(object, self)
    self.__index = self
    return object
end

player1 = Character:new()
player2 = Character:new()

player1.position.x = 0
player2.position.y = 10

print ("Player 1, position: ("
        .. player1.position.x .. ", " .. player1.position.y
        .. ", " .. player1.position.z .. ")")

print ("Player 2, position: ("
        .. player2.position.x .. ", " .. player2.position.y
        .. ", " .. player2.position.z .. ")")

if player1.position == player2.positon then
    print ("Player 1 and 2 have the same position reference");
else
    print ("Player 1 and 2 have unique positon tables");
end

print ("Table id:")
print ("Player 1: " .. tostring(player1.position))
print ("Player 2 :" .. tostring(player2.position))
print()

-- fix to the problem above
Character.new = function(self, object)
    object = object or {}

    -- Assign per instance variables after the object is valid
    -- but before setting the meta table! Copy all members of
    -- the new table by value!
    object.position = {}
    object.position.x = Character.position.x
    object.position.y = Character.position.y
    object.position.z = Character.position.z

    setmetatable(object, self)
    self.__index = self
    return object
end

-- Inheritance
-- Single inheritance
Animal = {
    sound = ""
}

Animal.new = function(self, object)
    object = object or {}
    setmetatable(object, self)
    self.__index = self
    return object
end

Animal.MakeSound = function(self)
    print(self.sound)
end

-- Dog is a class, not an object (instance)
Dog = Animal:new()
Dog.sound = "woof"
-- Cat is a class, not an Object (instance)
Cat = Animal:new()
Cat.sound = "meow"
Cat.angry = false
Cat.MakeSound = function(self)
    if self.angry then
        print("hissss")
    else
        print(self.sound)
    end
end

animals = { Cat:new(), Dog:new(), Cat:new() }
animals[1].angry = true

for i,v in ipairs(animals) do
    -- The current animal is stored in the v variable.
    -- It doesn't matter if the animal is a Dog or a Cat
    -- Both Dog and Cat extend Animal, which is guaranteed to contain a MakeSound function.
    v:MakeSound()
end
