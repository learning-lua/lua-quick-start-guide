-- Tables
-- Creating tables
tbl = {} -- Creates table, assigns it to tbl variable
print("The type of a table is: " .. type(tbl))
print()

-- Storing values
tbl = {}
tbl["x"] = 20
i = "x"

print (tbl["x"])
print (tbl[i])

tbl = {}

tbl["x"] = 10
tbl[10] = "x"

print ("x: " .. tbl["x"])
print ("10: " .. tbl[10])

tbl["x"] = 20
i = "x"

print (tbl["x"])
print (tbl[i])
print (tbl.x)

tbl.y = 10

print ("x + y: " .. tbl.x + tbl.y)
print (tbl["y"])
print (tbl.y)

tbl = {}
-- z is never added to the table!
print (tostring(tbl["z"])) -- nil
print (tostring(tbl.z)) -- nil
print()

-- Table constructor
colors = {
    red = "#ff0000",
    green = "#00ff00",
    blue = "#0000ff"
}
print ("red: " .. colors.red)
print ("green: " .. colors["green"])
print ("blue: " .. colors.blue)

colors = { r = "#ff0000", ["g"] = "#00ff00", [3] = "#0000ff"}
print ("red: " .. colors.r)
print ("green: " .. colors.g)
print ("blue: " .. colors[3])
print()

-- Tables are references
-- variables assigned by value
x = 10 -- y assigned 10 by value
y = x -- y assigned the value of x (10) by value

x = 15 -- x assigned 15 by value

print (x) -- 15
print (y) -- 10

-- assign a variable by reference
a = {} -- a is assigned a table reference
b = a -- b references the same table as x

b.x = 10 -- also creates a.x, a and b reference the same table
a.y = 20 -- also creates b.y, a and b reference the same table
a.x = 30 -- also changes b.x, a and b reference the same table

-- Even tough we assigned different variables to a.x and b.x
-- because the variables reference the same table, they should
-- both have the same value
print ("a.x: " .. a.x) -- prints a.x: 30
print ("b.x: " .. b.x) -- print b.x: 30

print ("a.y: " .. a.y) -- printx a.y: 20
print ("b.y: " .. b.y) -- prints b.y: 20

a = nil -- a no longer references the table
b = nil -- nothing references the table after this
print()
